#include <iostream>
#include <utility>
#include <map>
#include <vector>
#include <fstream>
#include <sstream>
#include <string>
#include <cmath>
#include <time.h>
using namespace std;
typedef pair<int,int> Node;
typedef map<Node, int> Orders;
Node center(35, 35);
int totalS;
double totalD;
int overScore;
double overDistance;
double rounding(double num, int index)
{
	int multiplier;
	multiplier = pow(10, index);
	num = (int)(num * multiplier + 0.5) / (multiplier * 1.0);
	return num;
}
double getDistance(const Node n1, const Node n2) // 兩點之間的距離
{
	double deltaX = 0, deltaY = 0, distance = 0;
	deltaX = (n1.first - n2.first) * (n1.first - n2.first);
	deltaY = (n1.second - n2.second) * (n1.second - n2.second);
	return rounding(pow((deltaX + deltaY), 0.5),2);
}
void findThebestNode(Orders& Data, Node n,double totaldistance,int totalscore) { //找到相對點之最佳移動點
	double distance = 9999, score = 0;
	if (totaldistance < 100) {
		Node best;
		Orders::iterator bestN = Data.begin();
		for (Orders::iterator it = Data.begin(); it != Data.end(); it++) {
			double d = getDistance(it->first, n);
			if (d<distance && (d+totaldistance) <= 100) {
				bestN = it;
				distance = getDistance(it->first, n);
				best = it->first;
				score = it->second;
			}
		}
		totaldistance += distance;
		totalscore += score;
		if (totaldistance < 100) {
			cout << "Go to visit (" << bestN->first.first << "," << bestN->first.second << ")\n";
			cout << "Distance :" << distance << endl;
			cout << "Score :" << score << endl << endl;
			Data.erase(bestN);
		}
		else {
			overDistance = distance;
			overScore = score;
		}
		findThebestNode(Data,best, totaldistance, totalscore);
	}
	else {
		totaldistance -= overDistance;
		totalscore -= overScore;
		totalD += totaldistance;
		totalS += totalscore;
		overDistance = 0;
		overScore = 0;
		cout << "Day Total Distance : " << totaldistance << endl;
		cout << "Day Total Score : " << totalscore << endl;
	};
}
int main()
{
	Orders Database;
	ifstream inFile("Q2data.csv");
	if (!inFile.fail())
	{
		// 輸入資料
		string input;
		while (getline(inFile, input))
		{
			istringstream iss(input);
			string xStr, yStr, ScoreStr;
			int x, y, Score;
			getline(iss, xStr, ',');
			getline(iss, yStr, ',');
			iss >> ScoreStr;
			x = stoi(xStr);
			y = stoi(yStr);
			Score = stoi(ScoreStr);
			Node node(x, y);
			Database[node] = Score;
		}
		Orders OriData = Database;
		Orders o;
		for (int i = 0; i < 4; i++) {
			cout << "---------Day " << i + 1 << "'s schedule:---------\n";
			findThebestNode(Database, center, 0, 0);
		}
		cout << "------------Summarize------------\n";
		cout << "Total Distance: " << totalD << endl;
		cout << "Total Score : " << totalS<<endl;
		cout <<"程式執行時間 :"<< (double)clock() / 1000 << "s\n";
		inFile.close();
		return 0;
	}
}